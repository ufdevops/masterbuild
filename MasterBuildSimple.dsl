// This create a DSL_Build_<repoName> for all team repos in gitbucket

import com.kronos.falcon.utilities.JenkinsJob
import com.kronos.falcon.utilities.KronosProperties

def addComponentScm(context, gitPrefix, repoName, theBranch, extraRepos) {
	// cannot have var named branch because it collides with scm branch method
  println("Adding git pull for ${repoName}")
  context.multiscm {
		// Add the git pull for the requested repo
    git{
      remote{
        url ("${gitPrefix}/${repoName}.git")
      }
      branch(theBranch)
			extensions {
				// Put this repo in its own subdirectory
				relativeTargetDirectory(repoName)
			}
    }
		// Add the git pull for the requested extra repos
		for (extraName in extraRepos) {
			println("Adding git pull for ${extraName}")
			git{
				remote{
					url(gitPrefix + "/" + extraName + ".git")
				}
				branch(theBranch)
				extensions {
					// Put this repo in its own subdirectory
					relativeTargetDirectory(extraName)
				}
			}
		}
  }
}

def addTriggers(context, cronExpression) {
  context.triggers{
		// Kick off all the DSL_Build_<repo> jobs when the seeder is done
    upstream('DSLSeeder', 'UNSTABLE')
		scm(cronExpression)
  }
}

def addLogRotator(context) {
	context.logRotator {
		daysToKeep(-1)
		numToKeep(50)
		artifactDaysToKeep(-1)
		artifactNumToKeep(50)
	}
}

def addWrappers(context) {
  context.wrappers{
    preBuildCleanup()
		timestamps()
		buildUserVars()
  }
}

def addDslJobs(context, targets, classpath) {
  context.steps {
    dsl {
      external(targets)
      removeAction('DELETE')
      additionalClasspath(classpath)
    }
  }
}

def runmain(context) {
	// Bitbucket parameters
	String protocol  = 'https:/'
	String address   = 'api.bitbucket.org'
	String version   = '2.0'
	String team      = 'ufdevops'
	String username  = "domenic_larosa@unifirst.com"
	String password  = "AtlasRocks99!"
	String gitPrefix = "git@bitbucket.org:" + team
	String userpass  = username + ":" + password;

	// Get a list of repos from the project
	String reposUrl = [protocol, address, version, "teams", team, "repositories?limit=10000"].join("/")
	URL stashUrl = reposUrl.toURL()
	URLConnection connection = stashUrl.openConnection()
	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
	connection.setRequestProperty ("Authorization", basicAuth)
	InputStream inputStream = connection.getInputStream()
	def repos = new groovy.json.JsonSlurper().parseText(inputStream.text)
	inputStream.close()
	
	// Loop over all repos in the project and create a DSL job for each
	repos.values.each {
		// Get the url to do the clone from the json.  It's the one for ssh
		def cloneUrl;
		it.links.clone.each {
			if (it.name=="ssh") {
				cloneUrl = it.href
			}
		}
		
		// Get the repo name from the clone url, basically strip the path and .git off it
		String repo = cloneUrl.tokenize( '/' ).last()
		String repoName = repo.tokenize('.')[0]
		String branch = 'master'
		// Don't create a build job fothese repos
		def excludedRepos = ['masterbuild', 'dlrutil', 'dsl-extension']
		// these repos will be checked out by the build job
		def extraRepos = ['dlrutil']
		
		if (!excludedRepos.contains(repoName)) {
			// Create a job to build the DSL in each repo
			def jobName = "DSL_Build_${repoName}"
			println ("Creating job: " + jobName)
			job(jobName) {
				addLogRotator(delegate)
				addComponentScm(delegate, gitPrefix, repoName, branch, extraRepos)
				addWrappers(delegate)
				addTriggers(delegate, "H H * * *")
				// TODO: the jars should be in Artifactory
				addDslJobs(delegate, "**/*.dsl", "dsl-extension/build/libs/*.jar")
			}
		}
		else {
			println("Skipping repo: " + repoName)
		}
	}
}

runmain()
